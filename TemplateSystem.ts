import * as http from 'http';
import express from 'express';
import cors from 'cors';
import * as util from 'util';
import { v4 as uuid } from 'uuid';
import compression from 'compression';
// import { Pool } from 'pg';

import bodyParser from 'body-parser';
import { Db } from 'mongodb';

import ErrorCodes from './lib/ErrorCodes';
import { ApiResponse, ApiErrorResponse } from './interfaces/Base';
import * as Routes from './routes';

import Security from './lib/Security';

import MongoConnection from './lib/MongoConnection';
// import PgSqlConn from './lib/PgSqlConn';
// import Cron from './lib/Cron';

const pjson = require('./package.json');

const appName = pjson.name;
const appVersion = pjson.version;
export { appName, appVersion };

export class TemplateSystem
{
    static config: any;

    args: Record<string, string>;

    env: string;

    etcd: any;

    // pgDb: Pool;

    userMongoClient: MongoConnection;

    userMdb: Db;

    constructor(config: Record<string, any>, args: Record<string, string>)
    {
        const me = this;

        TemplateSystem.config = config;

        me.args = args;

        me.env = args.env;

        me.startService();
    }

    async startService()
    {
        const me = this;
        try
        {
            await me.init();
            me.startTemplateSystemServer();
        }
        catch (e)
        {
            console.error(e);
        }
    }

    async init()
    {
        const me = this;

        me.userMongoClient = new MongoConnection();
        me.userMdb = await me.userMongoClient.getConnection(TemplateSystem.config.mongoUsersDb);

        // eslint-disable-next-line no-new
        new Security(TemplateSystem.config);

        // me.pgDb = await PgSqlConn.init(TemplateSystem.config);
        // eslint-disable-next-line no-new
        // new Cron();

        await me.createIndexes();
    }

    async initializeWeb(port)
    {
        const me = this;

        if (port === undefined)
        {
            throw (new Error('Please define web server port'));
        }

        console.log('Starting web server');

        const app = express();

        app.use(cors(
            {
                credentials: true,
                origin: true,
            },
        ));

        app.use(compression({ level: 1 }));

        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());

        app.use((err, req, res, next) =>
        {
            if (err)
            {
                console.warn(err);
                res.header('Content-type', 'application/json');
                res.end(JSON.stringify({
                    error: true,
                    code: 'request_parse_error',
                }));
            }
            else
            {
                next();
            }
        });

        http.createServer(app).listen(port, '0.0.0.0', () =>
        {
            console.info(`Express server listening on port ${port}`);
        });

        app.all('/*', async (req, res) =>
        {
            const startTime = new Date().getTime();

            res.header('Access-Control-Allow-Headers', 'X-Requested-With');

            let urlParams: Record<string, string> = {};
            let response : ApiResponse | ApiErrorResponse;
            let className: string;
            let method: string;
            let params: Record<string, any> = {};
            try
            {
                urlParams = req.params;
            }
            catch (err)
            {
                console.log(err);
            }

            const splitParams = urlParams[0].split('/');

            if (typeof req.url !== 'undefined')
            {
                try
                {
                    if (urlParams[1] == null)
                    {
                        // eslint-disable-next-line prefer-destructuring
                        urlParams[1] = splitParams[1];
                        // eslint-disable-next-line prefer-destructuring
                        urlParams[0] = splitParams[0];
                    }

                    if (Routes[urlParams[0].toLowerCase()] != null)
                    {
                        className = urlParams[0].toLowerCase();
                        method = urlParams[1].toLowerCase();
                    }

                    // auth middleware here
                    if (Routes[className].authLevel !== 0)
                    {
                        const jwt = req.headers?.cookie;
                        try
                        {
                            await Security.verifyJWT(jwt, Routes[className].authLevel);
                        }
                        catch (e)
                        {
                            if (e.code == null)
                            {
                                throw (new ErrorCodes('jwt_verify_error', {
                                    errorData: e,
                                    class: className,
                                }));
                            }
                            throw (e);
                        }
                    }

                    const instance = new Routes[className].Class(me, req, res);

                    if (req.method === 'GET')
                    {
                        params = req.query;
                    }
                    else
                    {
                        params = req.body;
                    }

                    if (method != null && method !== '')
                    {
                        response = await instance[method](params);
                    }
                    else
                    {
                        throw (new ErrorCodes('method_not_found', {
                            method: urlParams[1],
                            class: urlParams[0],
                        }));
                    }
                }
                catch (e)
                {
                    console.log(e);
                    if (typeof e.error !== 'undefined')
                    {
                        if (e.error === true && e.code === undefined)
                        {
                            response = {
                                error: true,
                                code: 'generic_error',
                                notice: 'GENERIC_ERROR',
                                params:
                                    {
                                        errorData: e,
                                    },
                            };
                        }
                        else
                        {
                            response = e;
                        }
                    }
                    else if (e.toString() === 'TypeError: instance[method] is not a function')
                    {
                        const error = new ErrorCodes('method_not_found', {
                            method: urlParams[1],
                            class: urlParams[0],
                        });

                        response = error;
                    }
                    else
                    {
                        response = {
                            error: true,
                            code: 'generic_error',
                            notice: 'GENERIC_ERROR',
                            params:
                                {
                                    errorData: e.toString(),
                                },
                        };
                    }
                }
                finally
                {
                    if (response !== undefined && response !== null)
                    {
                        res.header('Content-type', 'application/json');
                        if (typeof response === 'string')
                        {
                            res.end(response);
                        }
                        else
                        {
                            response.header = {
                                reqId: (params.header != null) ? params.header.reqId : null,
                                respId: uuid(),
                                respTimestamp: new Date().getTime(),
                                appName,
                                appVersion,
                            };
                            res.end(JSON.stringify(response));
                        }
                    }

                    console.info({
                        serviceMethod: method,
                        serviceErrorCode: response.code,
                        requestUuid: params.requestUuid,
                        ticketCode: params.ticketCode,
                        userId: null,
                        operationId: null,
                        req: params,
                        res: util.inspect(response).substring(0, 500),
                        time: new Date().getTime() - startTime,
                    });
                }
            }
        });
    }

    async startTemplateSystemServer()
    {
        const me = this;

        process.on('unhandledRejection', (reason, p) =>
        {
            console.error('Unhandled Rejection at: Promise', p, 'reason:', reason);
        });

        await me.initializeWeb(TemplateSystem.config.port);
    }

    async createIndexes()
    {
        const me = this;

        await me.userMdb.collection('users').createIndex(
            {
                username: 1,
            },
            {
                unique: true,
            },
        );
    }
}

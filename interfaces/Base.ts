/**
 * @typedef {Object} ApiErrorResponse
 * @property {boolean} error - Is error (true/false)
 * @property {string} code - Code of the error
 * @property {string} notice - Notice of the error
 * @property {object} params - Key->value params for use in notice
 * @property {ApiErrorResponse} errors - Additional errors if there are multiple
 */
export interface ApiErrorResponse extends ApiResponse {
    error : boolean;
    code : string;
    notice : string;
    params : Record<string, any>;
    errors? : ApiErrorResponse[];
}

export interface ApiResponse {
    header?: object,
    code?: string,
}

// extensions

type RequestCookieExtension = {
    headers: {
        authorization?: string,
    },
};

// Extended types

export type RequestExtended = Request & RequestCookieExtension;

## Name
Template project for quick set up of typescript node server

## Description
Use this project as a starting point for new typescript node project

## Config structure
```json
{
    "env": "local",
    "port": 8001,
    "mongoUsersDb":
    {
        "mongoUri": "mongodb://127.0.0.1:27017",
        "dbName": "users"
    },
    "security":
    {
        "jwtSecret": "thisIsASecretKeyAndShouldBeDeclaredOutsideThisJSON",
        "jwtExpireTime": "12h",
        "saltRounds": 10
    }
}
```

## UserModule
Every user must be defined with username that will be unique, password hash that will be generated from string password
and authLevel which will determine what rights user has.
Every route has an auth level. If users auth level is lower then the route they are trying to access error will be returned.

## MongoLocalCommands
start:
sudo systemctl start mongod

stop:
sudo systemctl stop mongod
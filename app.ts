/* eslint-disable max-len */
import { v4 as uuid } from 'uuid';
import { TemplateSystem } from './TemplateSystem';

const initApp = async () =>
{
    const args : Record<string, string> = {};

    for (const arg of process.argv)
    {
        const argVal = arg.split('=');
        if (argVal[1] !== undefined)
        {
            [, args[argVal[0]]] = argVal;
        }
    }

    if (args.env === undefined)
    {
        throw (new Error('No environment defined'));
    }

    args.service = 'template-system';
    args.uuid = uuid();

    let config = null;

    // override config for local development
    if (args.config !== undefined)
    {
        // eslint-disable-next-line import/no-dynamic-require,  global-require
        const configData = require(args.config);
        if (configData == null)
        {
            throw (new Error(`Config not found in ${args.config}, exiting`));
        }
        config = configData;
    }
    else
    {
        throw (new Error('No config defined'));
    }

    // eslint-disable-next-line no-new
    new TemplateSystem(config, args);
};

initApp();

/* eslint-disable no-undef */
const { default: axios } = require('axios');

const serverUrl = 'http://localhost:8001';

describe('Template unit test', () =>
{
    it('should confirm template test', async () =>
    {
        const data1 = await axios.post(`${serverUrl}/templateUrl`, {
            template: 'params',
        });

        expect(data1).toBeDefined();
        expect(data1.data.error).toBe(false);
        expect(data1.data.notice).toEqual('success');

        const data2 = await axios.post(`${serverUrl}/templateUrl2`, {
            reservationId: data1.data.template,
        });

        expect(data2).toBeDefined();
        expect(data2.data.error).toBe(false);
        expect(data2.data.notice).toEqual('success');
    });
});

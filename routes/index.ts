/* eslint-disable import/no-import-module-exports */
import { Template } from './Template';
import { Users } from './Users';

module.exports = {
    template: {
        Class: Template,
        authLevel: 1,
    },
    users: {
        Class: Users,
        authLevel: 0,
    },
};

import Base from './Base';
// eslint-disable-next-line import/prefer-default-export
export class Template extends Base
{
    async templateurl(params)
    {
        const me = this;
        return {
            error: false,
            params,
        };
    }
}

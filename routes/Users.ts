import Base from './Base';
import Security from '../lib/Security';
import ErrorCodes from '../lib/ErrorCodes';

interface LoginUser {
    username: string,
    password: string,
}

interface InsertUserParams {
    username: string,
    password: string,
}

// eslint-disable-next-line import/prefer-default-export
export class Users extends Base
{
    async finduser(params)
    {
        const me = this;
        const response = await me.lib.userMdb.collection('users').findOne(
            { username: params.username },
        );
        return response;
    }

    async register(params: InsertUserParams)
    {
        const me = this;
        const passwordHash = await Security.hashPassword(params.password);
        const user = {
            username: params.username,
            authLevel: 1,
        };
        const responseJWT = await Security.generateJWT(user);
        const responseInsert = await me.lib.userMdb.collection('users').insertOne(
            {
                username: params.username,
                passwordHash,
                authLevel: 1,
            },
        );
        const returnVal = {
            jwt: responseJWT,
            ...responseInsert,
        };
        return { error: false, data: returnVal };
    }

    async login(params: LoginUser)
    {
        const me = this;
        const user = await me.lib.userMdb.collection('users').findOne(
            { username: params.username },
        );

        if (user == null)
        {
            throw new ErrorCodes('object_not_found', {
                params: { username: params.username },
                type: user,
            });
        }

        if (await Security.comparePasswords(params.password, user.passwordHash))
        {
            delete user.passwordHash;
            const responseJWT = await Security.generateJWT(user);
            const returnVal = {
                jwt: responseJWT,
            };
            return { error: false, data: returnVal };
        }

        throw new ErrorCodes('password_incorrect', {
            params,
        });
    }

    async verify()
    {
        const me = this;
        const token = me.req.headers?.authorization.split(' ')[1];
        const verified = await Security.verifyJWT(token);
        return verified;
    }
}

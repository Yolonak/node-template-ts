import type { TemplateSystem } from '../TemplateSystem';
import { RequestExtended } from '../interfaces/Base';

/**
 * Base class - abstract class providing lib instance and language code to extending classes
 */
export default class Base
{
    config: Record<string, any>;

    lib: TemplateSystem;

    req: RequestExtended;

    res: Response;

    constructor(lib, req?, res?)
    {
        const me = this;

        me.req = req;
        me.res = res;

        me.config = lib.config;
        me.lib = lib;
    }
}

export default class Common
{
    static sleep(ms): Promise<void>
    {
        return new Promise((resolve) =>
        {
            setTimeout(() =>
            {
                resolve();
            }, ms);
        });
    }
}

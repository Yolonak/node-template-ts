export default class Cron
{
    constructor()
    {
        const me = this;
        // me.templatePeriodicJob();
    }

    async templatePeriodicJob()
    {
        const me = this;
        try
        {
            console.log('JOB HERE');
        }
        catch (error)
        {
            console.warn(error);
        }
        finally
        {
            setTimeout(async () =>
            {
                await me.templatePeriodicJob();
            }, 60000 * 60 * 24);
        }
    }
}

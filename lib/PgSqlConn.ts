// template postgre connection class
import { Pool } from 'pg';

export default class PgSqlConn
{
    static async init(config) : Promise<Pool>
    {
        try
        {
            const pgConfig = config.pgDb;

            const pgDb = new Pool({
                host: pgConfig.host,
                port: pgConfig.port,
                user: pgConfig.username,
                password: pgConfig.password,
                database: pgConfig.dbname,
            });

            pgDb.query = async function query(...args)
            {
                const meSec = this;
                args[0] = args[0].replace(/`/g, '');
                if (args[1] !== undefined)
                {
                    const argsIn = [];
                    if (Array.isArray(args[1]))
                    {
                        const regex = /[?]+/gm;
                        let n = 0;
                        args[0] = args[0].replace(regex, () =>
                        {
                            n += 1;
                            return `$${n}`;
                        });
                        for (const val of args[1])
                        {
                            argsIn.push(val);
                        }
                        args[1] = argsIn;
                    }
                    else
                    {
                        const regex = /(?<![:]):[a-zA-Z0-9_]+/gm;
                        let n = 0;
                        args[0] = args[0].replace(regex, (match) =>
                        {
                            n += 1;
                            argsIn.push(args[1][match.substring(1)]);
                            return `$${n}`;
                        });
                        args[0] = args[0].replace(/ LIKE/g, '::text LIKE');
                        args[1] = argsIn;
                    }
                }

                const result = await Object.getPrototypeOf(this).query.call(meSec, ...args);
                return result.rows;
            };

            // TODO args check if works
            pgDb.connect = async function connect(...args)
            {
                const meSec = this;
                const client = await Object.getPrototypeOf(this).connect.call(meSec, ...args);
                if (client != null)
                {
                    // eslint-disable-next-line @typescript-eslint/no-shadow
                    client.query = async function query(...args)
                    {
                        const meTrd = this;
                        args[0] = args[0].replace(/`/g, '');
                        if (args[1] !== undefined)
                        {
                            const argsIn = [];
                            if (Array.isArray(args[1]))
                            {
                                const regex = /[?]+/gm;
                                let n = 0;
                                args[0] = args[0].replace(regex, () =>
                                {
                                    n += 1;
                                    return `$${n}`;
                                });
                                for (const val of args[1])
                                {
                                    argsIn.push(val);
                                }
                                args[1] = argsIn;
                            }
                            else
                            {
                                const regex = /(?<![:]):[a-zA-Z0-9_]+/gm;
                                let n = 0;
                                args[0] = args[0].replace(regex, (match) =>
                                {
                                    n += 1;
                                    argsIn.push(args[1][match.substring(1)]);
                                    return `$${n}`;
                                });
                                args[0] = args[0].replace(/ LIKE/g, '::text LIKE');
                                args[1] = argsIn;
                            }
                        }

                        const result = await Object.getPrototypeOf(meTrd).query.call(meTrd, ...args);
                        return result.rows;
                    };
                }

                return client;
            };

            return pgDb;
        }
        catch (error)
        {
            console.error(error);
            return null;
        }
    }
}

export default class ErrorCodes extends Error
{
    static values = {

        method_not_found: {},
        object_not_found: {},

        // auth errors
        password_incorrect: {},
        jwt_verify_error: {},
        no_privilage: {},
    };

    error: boolean = null;

    code: string = null;

    notice: string = null;

    errors: ErrorCodes[] = null;

    params: any = {};

    constructor(key, params: any = null)
    {
        super(key);

        if (ErrorCodes.values[key] === undefined)
        {
            this.error = true;
            this.code = 'generic_error';
            this.notice = 'GENERIC_ERROR';
            const details = (params != null && params.params != null) ? (params.params) : null;
            this.params = {
                code: key,
                details,
            };
            return;
        }

        let notice = key.toUpperCase();
        if (ErrorCodes.values[key].notice !== undefined)
        {
            notice = ErrorCodes.values[key].notice;
        }

        if (params.length)
        {
            const errors = [];
            for (const item of params)
            {
                errors.push(new ErrorCodes(item.code, item.params));
            }

            this.error = true;
            this.code = key;
            this.notice = notice;
            this.errors = errors;
            return;
        }

        if (ErrorCodes.values[key].params !== undefined)
        {
            // eslint-disable-next-line no-param-reassign
            params = { ...params, ...ErrorCodes.values[key].params };
        }

        this.error = true;
        this.code = key;
        this.notice = notice;
        this.params = params;
    }

    // eslint-disable-next-line class-methods-use-this
    extend(vals)
    {
        ErrorCodes.values = { ...ErrorCodes.values, ...vals };
    }
}

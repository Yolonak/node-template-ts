import { MongoClient } from 'mongodb';

export default class MongoConnection
{
    // consider adding static property for default connection to prevent multiple connections
    client: MongoClient;

    options: object;

    constructor()
    {
        const me = this;
        me.client = null;
        me.options = {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            maxPoolSize: 20,
            minPoolSize: 10,
        };
    }

    async getConnection(mongoObject: any)
    {
        const me = this;
        if (!me.client)
        {
            me.client = await MongoClient.connect(mongoObject.mongoUri, me.options);
        }

        const mdb = await me.client.db(mongoObject.dbName);
        return mdb;
    }
}

import { sign, verify, SignOptions } from 'jsonwebtoken';
import ErrorCodes from './ErrorCodes';

const bcrypt = require('bcrypt');

export default class Security
{
    static secret: string;

    static expireTime: string;

    static saltRounds: number;

    constructor(config)
    {
        Security.secret = config.security.jwtSecret;
        Security.expireTime = config.security.jwtExpireTime;
        Security.saltRounds = config.security.saltRounds;
    }

    static async generateJWT(user: object)
    {
        const options: SignOptions = {
            algorithm: 'HS256',
            expiresIn: Security.expireTime,
        };
        const jwt = await sign({ data: user }, Security.secret, options);
        return jwt;
    }

    static async verifyJWT(jwt, authLevel = 0)
    {
        // decode jwt and check if auth level OK
        const res = await verify(jwt, Security.secret);
        if (res.data.authLevel == null || res.data.authLevel < authLevel)
        {
            throw (new ErrorCodes('no_privilage', {
                requiredAuthLevel: authLevel,
                userAuthLevel: res.data.authLevel,
            }));
        }
        return res;
    }

    static async hashPassword(password)
    {
        const hash = await bcrypt.hash(password, Security.saltRounds);
        return hash;
    }

    static async comparePasswords(password1: string, password2: string)
    {
        const match = await bcrypt.compare(password1, password2);
        return match;
    }
}
